DotOS Oreo - How to Build?
-------------

![DotOS logo](https://raw.githubusercontent.com/samgrande/XDA_Template/master/vendorart.png)

```
1. mkdir -p dotos

2. cd dotos

3. Initialize your local repository using the DotOS trees, use a command
  repo init -u git://github.com/DotOS/manifest.git -b dot-o

4. Clone my repo:
  git clone https://gitlab.com/getawaycar/isolatedworld/dotOS/local_manifests.git -b dot-o .repo/local_manifests

5. Sync the repo:
  repo sync --no-tags --no-clone-bundle --force-sync -c

6. To build:
  . build/envsetup.sh
  lunch (choose i9100)
  brunch i9100
```


Credits
-------
* [**rINanDO**](https://github.com/rINanDO)
* [**DotOS**](https://github.com/DotOS)
* [**LineageOS**](https://github.com/LineageOS)